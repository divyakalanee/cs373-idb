<b>Canvas/Discord Group Number: </b> 11-10 <br>
<b>Names of the team members: </b> Divya Kalanee, Timothy Zhang, Grant Martin, Jackson Nakos, Minh Nguyen <br>
<b>Name of the project: </b> concertfor.me <br>
<b>URL of the GitLab repo: </b> https://gitlab.com/jacksonnakos/cs373-idb <br>
<b>PLANNING DOC: </b> https://docs.google.com/document/d/1fdVCiuxDQEL7r3aggTBHsDoGL9jbHAEjySVgen2xi7c/edit?usp=sharing <br>
<b> The proposed project: </b> Concertsfor.me will be a webpage that allows music lovers to see their favorite artists' upcoming concerts. It will also help users discover more artists and songs that they would like based off of their current interests and genre preferences. It will aggregate and display information about upcoming concerts, from local scenes to large music festivals, in an easy to use fashion. <br>

<b> URLs of at least three disparate data sources: </b> <br>

Spotify for information on artists & their songs: https://developer.spotify.com/documentation/web-api/ <br>
Apple Music API for songs  : https://developer.apple.com/documentation/applemusicapi/songs <br>
Songkick for the concerts: https://www.songkick.com/developer/ <br>

<b> Three models:  </b> Concerts, Artists, Songs <br>
<b> An estimate of the number of instances for each model: </b> <br>

Concerts: 50 K 
Artists: 11 million
Songs: 100 K

<b> Data model attributes (to sort by): </b>

Concerts: location, dates, event venue, artists, price<br>
Artists: monthly active listeners, genre, location, names (alphabetically), age<br>
Songs: length, number of streams, genre, rating, release date

<b> Data model attributes (per instance): </b>

Concerts: occupancy of event venue, openers, nearby concerts(date, location), seat availability, nearby hotels <br>
Artists: photo of artist, name of artist, top songs by artist, language, albums, related artists<br>
Songs: artist, producer, label, lyrics, album, instruments

<b> Data model connections: </b>

Concerts: <br>

 -> Artists: Artists perform at concerts for their fans. <br>
 -> Songs: Songs are played at concerts by artists that their fans want to see. <br>

Artists: <br>

 -> Concerts: Artists perform at concerts (can link by artist, genre) <br>
 -> Songs: Artists have discographies (genre, artist, album) <br>

Songs: <br>

 -> Artists: Artists create songs (genre, album) <br>
 -> Concerts: Songs are performed at concerts (linked by artist) <br>

<b> Media for each model: </b>

Artists: photos of artist, social media feed, discography list <br>
Songs: album cover, youtube music video link, lyrics <br>
Concerts: list of upcoming concerts, nearby concerts, local concert venues, pictures, google maps location pin <br>

<b> What 3+ questions can be answered with this site? </b>

What upcoming concerts are near me that I can attend? <br>
What concerts are coming up for my favorite artists? <br>
What are some other songs or artists I would be interested in? <br>
What are some local artists or bands I can listen to? <br>
