import { render, screen, cleanup } from '@testing-library/react';
import Concerts from '../routes/Concerts'
import Artists from '../routes/Artists'
import Songs from '../routes/Songs'

import UncontrolledExample from '../components/Carousel';
import ListOfIndividualAbout from '../components/ListOfIndividualAbout';
import About from '../routes/About'
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';
import SearchCard from '../components/SearchCard'

import ListApiCards from '../components/ListApiCards';
import ListToolCards from '../components/ListToolCards';
import Nav from '../components/Nav'
import { findContributorInfoWithName, getTotalCommits, responseToContributorArray, statsCallResponseToTotalNumIssues } from '../utils/GitLabUtils'

describe('General checks', () => {


    test('Navbar', () => {
        const tree = renderer.create(<Nav />).toJSON()
        expect(tree).toMatchSnapshot()
    })
})



describe('About tests', () => {
    test('About', () => {
        const tree = renderer.create(<About />).toJSON()
        expect(tree).toMatchSnapshot()
    })
    test('APICards', () => {
        const tree = renderer.create(<ListApiCards />).toJSON()
        expect(tree).toMatchSnapshot()
    });
    test('ToolCards', () => {
        const tree = renderer.create(<ListToolCards />).toJSON()
        expect(tree).toMatchSnapshot()
    });
    test('ListOfIndividualAbout', () => {
        const tree = renderer.create(<ListOfIndividualAbout />).toJSON()
        expect(tree).toMatchSnapshot()
    });
    test('getTotalCommits', () => {
        let contibrutorInfoResponse = '[{"name":"Nhat Nguyen","email":"nhminh.ng@gmail.com","commits":2,"additions":0,"deletions":0},{"name":"Jackson Mickey Nakos","email":"jacksonnakos@utexas.edu","commits":12,"additions":0,"deletions":0},{"name":"Grant Martin","email":"grantmartin2002@gmail.com","commits":20,"additions":0,"deletions":0},{"name":"Minh Nguyen","email":"nhminhng@gmail.com","commits":26,"additions":0,"deletions":0},{"name":"Divya Kalanee","email":"divyakalanee@utexas.edu","commits":27,"additions":0,"deletions":0},{"name":"zhangtimothy","email":"37489234+zhangtimothy@users.noreply.github.com","commits":42,"additions":0,"deletions":0}]'
        let contributorArray = responseToContributorArray(contibrutorInfoResponse)
        expect(getTotalCommits(contributorArray)).toEqual(129);
    })
    test('findContributorInfoWithName', () => {
        let contibrutorInfoResponse = '[{"name":"Nhat Nguyen","email":"nhminh.ng@gmail.com","commits":2,"additions":0,"deletions":0},{"name":"Jackson Mickey Nakos","email":"jacksonnakos@utexas.edu","commits":12,"additions":0,"deletions":0},{"name":"Grant Martin","email":"grantmartin2002@gmail.com","commits":20,"additions":0,"deletions":0},{"name":"Minh Nguyen","email":"nhminhng@gmail.com","commits":26,"additions":0,"deletions":0},{"name":"Divya Kalanee","email":"divyakalanee@utexas.edu","commits":27,"additions":0,"deletions":0},{"name":"zhangtimothy","email":"37489234+zhangtimothy@users.noreply.github.com","commits":42,"additions":0,"deletions":0}]'
        let contributorArray = responseToContributorArray(contibrutorInfoResponse)
        var contributorInfo = findContributorInfoWithName(contributorArray, "Nhat Nguyen")
        expect(contributorInfo.email).toEqual('nhminh.ng@gmail.com')
    })
    test('statsCallResponseToTotalNumIssues', () => {
        expect(statsCallResponseToTotalNumIssues('{"statistics":{"counts":{"all":89,"closed":67,"opened":22}}}')).toEqual(89)
    })

})

describe('Home tests', () => {
    test('Carousel', () => {
        const tree = renderer.create(<UncontrolledExample />).toJSON()
        expect(tree).toMatchSnapshot()
    })
})

// cant test songs page due to weird bug, but filter and sort is 
// still snapshotted for concerts and artists filter, search, and sort features, and songs search is captured in the searchCard tests
describe('Search, Filter, Sort pages', () => {
    test('Search page', () => {
        const tree = renderer.create(<SearchCard />).toJSON()
        expect(tree).toMatchSnapshot()
    });
    test('Concerts', () => {
        const tree = renderer.create(<Concerts />).toJSON()
        expect(tree).toMatchSnapshot()
    });
    test('Artists', () => {
        const tree = renderer.create(<Artists />).toJSON()
        expect(tree).toMatchSnapshot()
    });


})



