import React from 'react'
import { Helmet } from 'react-helmet';
import SearchCard from '../components/SearchCard';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';

const Search = () => {
  return (
    <div>Songs
      <Helmet>
        <title>Find Songs</title>
      </Helmet>
      <div><SearchCard/></div>
    </div>
  )
}

  export default Search