import React, { Component, useState, useEffect } from 'react'
import * as d3 from "d3";


interface IProps {
    map: Map<any, any>
}

interface IState {

}
class BarGraphVisualization extends React.Component<IProps, IState> {
    ref!: SVGSVGElement;


    public map: Map<any, any>;


    constructor(props: IProps) {
        super(props)
        this.map = props.map;
    }

    public buildGraph() {
        console.log('BUILDING1')
        var svg = d3.select(this.ref);
        // count stuff up in a map
        // [genre, count of artists with that genre]
        var data = this.map;

        var barHeight = 20;

        var bar = svg
            .selectAll('rect')
            .data(data)
            .enter()
            .append('rect')
            .attr('width', function (d) { return d[1] * 10; })
            .attr('height', barHeight - 1)
            .attr('transform', function (d, i) {
                return "translate(225," + i * barHeight + ")";
            });

        var labels = svg
            .selectAll('text')
            .data(data)
            .enter()
            .append('text')
            .text(function (d) { return d[0] + " (" + d[1] + ")"; })
            .attr('transform', function (d, i) {
                return "translate(0," + (i * barHeight + barHeight / 2 + 3) + ")";
            });

    }

    componentDidMount() {
        // activate   
        this.buildGraph();
    }

    render() {
        return (<div className="svg">
            <svg className="container" ref={(ref: SVGSVGElement) => this.ref = ref} width='100' height='100000'></svg>
        </div>);
    }
}



export default BarGraphVisualization;
