Group Members (UT EID, Gitlab ID): 
- Jackson Nakos: jn27746, jacksonnakos
- Minh Nguyen: nn7294, nhminhng
- Divya Kalanee: dk27966, divyakalanee
- Timothy Zhang: tz3723, zhang.timothy
- Grant Martin: glm2367, grantmartin2002

Git SHA: f1098ef8a97db72a015022366d160eea894b659c

Project Leader: Jackson Nakos

GitLab Pipelines: https://gitlab.com/jacksonnakos/cs373-idb/-/pipelines

Website: https://www.concertfor.me/

Presentation: https://youtu.be/FPmCq0AX_8c 

Estimated completion time for each member: 
- Jackson Nakos: 10 hours
- Minh Nguyen: 10 hours
- Divya Kalanee: 10 hours
- Timothy Zhang: 10 hours
- Grant Martin: 15 hours

Actual completion time for each member:
- Jackson Nakos: 8 hours
- Minh Nguyen: 7 hours
- Divya Kalanee: 8 hours
- Timothy Zhang: 7 hours
- Grant Martin: 15 hours

Comments: 
Followed pagination tutorial from: https://www.youtube.com/watch?v=37xPlDBaA4A&ab_channel=Grepsoft 
Selenium Tests from: https://gitlab.com/JohnPowow/animalwatch/-/blob/main/frontend/gui_tests/runSeleniumTests.py
Yml File from: https://gitlab.com/AlejandroCantu/group2/-/blob/new-main/.gitlab-ci.yml     and:  https://gitlab.com/JohnPowow/animalwatch/-/blob/main/.gitlab-ci.yml
