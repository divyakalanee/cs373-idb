"""
This provides an API to provide Artist information.
Internally, it relies on the Spotify API.
https://developer.spotify.com/documentation/web-api/

Rreferences:
https://medium.com/@maxtingle/getting-started-with-spotifys-api-spotipy-197c3dc6353b
"""
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import json
import requests
import time


cid = "fb9866d5d41d401d8861da07051201d8"
secret = "6f449d4565744d25bd9dd9dc7250ee7c"
client_credentials = SpotifyClientCredentials(client_id=cid, client_secret=secret)
sp = spotipy.Spotify(client_credentials_manager=client_credentials)


def __get_raw_artists(n):
    year = 2022
    raw_artists = []
    while n > 0:
        lim = min(n, 50)
        res = sp.search(
            q=f"year:{year}", type="artist", limit=lim, offset=len(raw_artists)
        )
        raw_artists.extend(res["artists"]["items"])
        n -= lim
    return raw_artists


def __extract_fields(raw_artist):
    # TODO: revert this
    rd = raw_artist
    # print(json.dumps(rd, indent=2))
    d = {}
    d["artist_name"] = rd["name"]
    try:
        d["spotify_page_url"] = rd["external_urls"]["spotify"]
    except:
        d["spotify_page_url"] = "https://open.spotify.com/"
    try:
        d["followers"] = rd["followers"]["total"]
    except:
        d["followers"] = 0
    d["popularity"] = rd["popularity"]
    try:
        d["genre"] = rd["genres"][0] if len(rd["genres"]) > 0 else "None Tagged"
    except:
        d["genre"] = "None Tagged"
    d["spotify_id"] = rd["id"]
    try:
        d["image_url"] = rd["images"][0]["url"]
    except:
        d[
            "image_url"
        ] = "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
    return d


def __make_list(raw_artists):
    names_seen = set()
    artists = []
    for ra in raw_artists:
        artist = __extract_fields(ra)
        if artist["name"] not in names_seen:
            names_seen.add(artist["name"])
            artists.append(artist)
    return artists


# THIS IS THE PUBLIC FUNCTION
def get_artists(n=10):
    raw_artists = __get_raw_artists(n)
    artists = __make_list(raw_artists)
    result = {"artists": artists}
    return json.dumps(result, indent=2)


def __get_artist_by_name(name):
    results = sp.search(q="artist:" + name, type="artist")
    top_res = results["artists"]["items"][0]
    artist_dict = __extract_fields(top_res)
    # res = json.dumps(artist_dict, indent=2)
    # print(res)
    return artist_dict


def get_artist(name):
    results = sp.search(q="artist:" + name, type="artist")
    top_res = results["artists"]["items"][0]
    return top_res["name"]


def get_field(dict, s):
    try:
        return dict[s]
    except:
        return "Not Found"


def __enrich_artist(artist_dict):
    # need to get birthday, aliases, gender, home country
    name = artist_dict["artist_name"]
    q = name
    url = "https://musicbrainz.org/ws/2/artist"
    params = {"query": q, "limit": 1, "fmt": "json"}

    response = requests.get(url, params=params)
    data = response.json()
    if "artists" not in data or len(data["artists"]) == 0:
        gender = "Not Found"
        country = "Not Found"
        bday = "2000-01-01"
        aliases = "Not Found"
    else:
        data = data["artists"][0]
        gender = get_field(data, "gender")
        country = get_field(data, "country")
        try:
            bday = data["life-span"]["begin"].strip()
        except:
            bday = "2000-01-01"
        try:
            aliases = [d["name"].strip() for d in data["aliases"]]
            aliases = ", ".join(aliases)
            if len(aliases) > 100:
                aliases = aliases[:100] + "..."
        except:
            aliases = "Not Found"
    artist_dict["birthday"] = bday
    artist_dict["aliases"] = aliases
    artist_dict["gender"] = gender
    artist_dict["home_country"] = country
    return artist_dict


# returns dict with artists for the names, unique by name and artist
def get_artists_by_name(names):
    names = set(names)
    artists = []
    seen_ids = set()
    for name in names:
        artist = __get_artist_by_name(name)
        aid = artist["spotify_id"]
        if aid in seen_ids:
            continue
        seen_ids.add(aid)
        artist = __enrich_artist(artist)

        for k in artist:
            if isinstance(artist[k], str):
                artist[k] = artist[k].replace("'", "").replace('"', "")

        artists.append(artist)
        time.sleep(0.1)

    result = {"artists": artists}
    return result


# USAGE
def test():
    # res = get_artists(n=25)
    # print(res)
    print(get_artists_by_name(["Harry Styles", "Post Malone", "Marshmello"]))


def test_wiki():
    subject = "Drake"
    url = "https://en.wikipedia.org/w/api.php"
    params = {
        "action": "query",
        "format": "json",
        "titles": subject,
        "prop": "extracts",
        "exintro": True,
        "explaintext": True,
    }

    response = requests.get(url, params=params)
    data = response.json()

    page = next(iter(data["query"]["pages"].values()))
    # print(json.dumps(page, indent=2))
    txt = page["extract"].strip()
    print(f"BIO: {txt}")
    born_i = txt.find("born")
    born_end = txt.find(")", born_i)
    bday_str = txt[born_i + 5 : born_end].strip()
    print(f"bday: {bday_str}")


if __name__ == "__main__":
    # test()
    # print(get_artists(150))
    # get_artists(3)
    # test_wiki()
    res = get_artists_by_name(
        ["Times Square Church", "James Ducker", "Dean M. Parker", "The Rolling Stones"]
    )
    print(json.dumps(res, indent=2))
