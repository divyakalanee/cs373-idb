import psycopg2

# https://pynative.com/python-postgresql-tutorial/#h-python-postgresql-database-connection
# https://www.tutorialspoint.com/python_data_access/python_postgresql_database_connection.htm
conn = psycopg2.connect(
    database="postgres",
    user="Structure3081",
    password="8oCKgqY7Ns6phDZBatQR",
    host="concertformedb.cfvcdcztpu5h.us-east-2.rds.amazonaws.com",
    port="5432",
)

cursor = conn.cursor()

# Executing an MYSQL function using the execute() method
cursor.execute("select version()")

# Fetch a single row using fetchone() method.
data = cursor.fetchone()
print("Connection established to: ", data)

cursor.execute("SELECT * FROM songs")
data = cursor.fetchone()
print("DATA: ", data)

# TODO: insert stuff, get multiple values
s = "INSERT INTO concerts VALUES \
    (1, 'Love On Tour', 'Madison Square Garden', '100 Main St.', \
    'Harry Styles', '10/5/2023', '18:00', 'CT', 'google.com')"
# cursor.execute(s)
conn.commit()

# Delete everything from the table
s = "DELETE FROM songs"
cursor.execute(s)
conn.commit()

# Closing the connection
cursor.close()
conn.close()
